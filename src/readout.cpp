#include <stdio.h>
#include <iostream>
#include <fstream>
#include <pcap.h>
#include <time.h>

#include "CLI11.hpp"
#include <fmt/core.h>

typedef struct ip_address {
    u_char byte1;
    u_char byte2;
    u_char byte3;
    u_char byte4;
} ip_address;

typedef struct ip_header {
    u_char  ver_ihl;        // Version (4 bits) + Internet header length (4 bits)
    u_char  tos;            // Type of service
    u_short tlen;           // Total length
    u_short identification; // Identification
    u_short flags_fo;       // Flags (3 bits) + Fragment offset (13 bits)
    u_char  ttl;            // Time to live
    u_char  proto;          // Protocol
    u_short crc;            // Header checksum
    ip_address  saddr;      // Source address
    ip_address  daddr;      // Destination address
    u_int   op_pad;         // Option + Padding
} ip_header;

typedef struct udp_header {
    u_short sport;          // Source port
    u_short dport;          // Destination port
    u_short len;            // Datagram length
    u_short crc;            // Checksum
} udp_header;

typedef struct frame_counter {
    uint8_t byte3;
    uint8_t byte2;
    uint8_t byte1;
    uint8_t byte0;
} frame_counter;

typedef struct data_header {
    char byte3;
    char byte2;
    char byte1;
    char byte0;
} data_header;

typedef struct header_info {
    uint16_t evbld_datalength;
    uint16_t trig_count;
} header_info;


int main(int argc, char *argv[]) {

    std::string device;
    std::string out_prefix;
    bool offline{false};

    CLI::App app{"Save SRS APV data to binary file"};
    app.add_option("device", device, "Ethernet card name")->required();
    app.add_option("output", out_prefix, "Output file prefix")->required();
    app.add_flag("--offline", offline, "Read data from pcap file instead of network card");
    try {
        app.parse(argc, argv);
    } catch (const CLI::ParseError &e) {
        return app.exit(e);
    }

    fmt::print("Reading from device {}...\n", device);

    char errbuf[PCAP_ERRBUF_SIZE];
    struct bpf_program fp;
    char filter_exp[] = "port 6006";
    bpf_u_int32 mask;
    bpf_u_int32 net;

    if (pcap_lookupnet(device.c_str(), &net, &mask, errbuf) == -1) {
        fmt::print("Can't get netmask for device {}\n", device.c_str());
        net = 0;
        mask = 0;
    }
    pcap_t *handle;
    if (offline) {
        handle = pcap_open_offline(device.c_str(), errbuf);
    } else {
        handle = pcap_open_live(device.c_str(), BUFSIZ, 1, 1000, errbuf);
    }
    if (handle == NULL) {
        fmt::print("Couldn't open device {}: {}\n", device.c_str(), errbuf);
        return 2;
    }
    if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
        fmt::print("Couldn't parse filter {}: {}\n", filter_exp, pcap_geterr(handle));
        return 2;
    }
    if (pcap_setfilter(handle, &fp) == -1) {
        fmt::print("Couldn't install filter {}: {}\n", filter_exp, pcap_geterr(handle));
        return 2;
    }

    struct pcap_pkthdr header;
	const u_char *packet;

    /**
     * Store files and counters in map
     * using FEC IP as key
     */
    uint32_t fec_ip;
    std::map<uint32_t, std::FILE *> raw_files;
    std::map<uint32_t, int> fec_counters;
    std::map<uint32_t, int> event_counters;

    for (int packet_count=0; true; packet_count++) {
        packet = pcap_next(handle, &header);
        if (packet==NULL) {
            break;
        }
        //fmt::print("Received a packet with length {}\n", header.len);

        size_t payload_size = header.len;

        ip_header *ih = (ip_header *) (packet + 14);
        payload_size -= 14;
        int ip_len = (ih->ver_ihl & 0xf) * 4;
        udp_header *uh = (udp_header *) ((u_char *) ih + ip_len);
        payload_size -= ip_len;
        int sport = ntohs(uh->sport);
        int dport = ntohs(uh->dport);

        fec_ip = ih->saddr.byte1 << 24 | ih->saddr.byte2 << 16 | ih->saddr.byte3 << 8 | ih->saddr.byte4;
        /**
         * Open raw binary file for this FEC
         * if it doesn't exist in the map yet
         */
        try {
            raw_files.at(fec_ip);
        } catch (std::out_of_range) {
            raw_files[fec_ip] = std::fopen(fmt::format("{}-{:x}.raw", out_prefix, fec_ip).c_str(), "wb");
        }

        /*fmt::print("Packet from {}.{}.{}.{}:{} to {}.{}.{}.{}:{}\n",
                ih->saddr.byte1, ih->saddr.byte2, ih->saddr.byte3, ih->saddr.byte4, sport,
                ih->daddr.byte1, ih->daddr.byte2, ih->daddr.byte3, ih->daddr.byte4, dport
                );*/

        uint32_t *first_word = (uint32_t *) ((uint8_t *) uh + sizeof(udp_header));
        //fmt::print("  First word: {:#x}\n", *first_word);
        /**
         * This is the event trailer, mark the event as ended
         */
        if (*first_word==0xfafafafa) {
            //fmt::print("End of event");
            event_counters[fec_ip]++;
        } else {
            frame_counter *srs_frame_counter = (frame_counter *) ((uint8_t *) uh + sizeof(udp_header));
            size_t wrote_bytes = std::fwrite(srs_frame_counter, 1, sizeof(srs_frame_counter), raw_files[fec_ip]);
            payload_size -= sizeof(udp_header);
            //fmt::print("  Timestamp {}{}{}, frame {}\n", srs_frame_counter->byte3, srs_frame_counter->byte2, srs_frame_counter->byte1, srs_frame_counter->byte0);

            data_header *srs_data_header = (data_header *) ((uint8_t *) srs_frame_counter + 4);
            payload_size -= 4;
            //fmt::print("  Data type {}{}{}, APV {}\n", srs_data_header->byte3, srs_data_header->byte2, srs_data_header->byte1, (int) srs_data_header->byte0);

            header_info *srs_hi = (header_info *) ((uint8_t *) srs_data_header + 4);
            payload_size -= 4;
            //fmt::print("  Trig counter {}, data length {}, payload length {}\n", srs_hi->trig_count, srs_hi->evbld_datalength, payload_size);
            fec_counters[fec_ip] = srs_hi->trig_count;

            /*std::ofstream ofile{fmt::format("/tmp/udp/{}.txt", packet_count)};
            uint8_t *dxl = (uint8_t *) ((uint32_t *) srs_hi + 1);
            uint8_t *dxm = dxl + 1;
            for (int i=0; i<payload_size; i++) {
                uint16_t dx = *dxm * 256 + *dxl;
                ofile << dx << "\n";
                dxl += 2;
                dxm += 2;
            }
            ofile.close();*/
        }

        if (packet_count%1000==0) {
            fmt::print("\rTrigger counters ");
            for (auto fec_pair:fec_counters) {
                fmt::print("| FEC {:#x} trigger {} event {} ", fec_pair.first, fec_pair.second, event_counters[fec_pair.first]);
            }
        }
    }
    fmt::print("\n");

	pcap_close(handle);

    return 0;
}
