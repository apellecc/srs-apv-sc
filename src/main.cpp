#include <iostream>
#include <iomanip>

#include <string.h>

#include "CLI11.hpp"

#include "FEC.h"

int main(int argc, char *argv[]) {

    CLI::App app{"App description"};

    std::string hostname, config_path;
    bool verbose = false;
    bool external_trigger = false;
    bool acquisition_on = false;
    app.add_option("hostname", hostname, "FEC hostname");
    app.add_option("config", config_path, "Path of the configuration file created by SRSDCS");
    app.add_flag("-v,--verbose", verbose, "Print out requests and replies");
    app.add_flag("--external-trigger", external_trigger, "Enable external trigger");
    app.add_flag("--acquisition-on", acquisition_on, "Enable data acquisition");
    CLI11_PARSE(app, argc, argv);

    FEC fec{hostname};
    fmt::print("Configuring FEC {} from file...\n", hostname);
    fec.configure_from_file(config_path, verbose);

    if (external_trigger) {
        fmt::print("Enabling external trigger...\n");
        std::string trigger_response = fec.set_external_trigger(true);
        if (verbose) {
            fmt::print("Response: {}\n", trigger_response);
        }
        sleep(0.5);
    }

    if (acquisition_on) {
        fmt::print("Enabling acquisition...\n");
        std::string acquisition_response = fec.set_readout_enable(true);
        if (verbose) {
            fmt::print("Response: {}\n", acquisition_response);
        }
        sleep(0.5);
    }

    fmt::print("FEC {} configured.\n", hostname); 

    return 0;
}
