#include <vector>
#include <arpa/inet.h>

#include <fmt/core.h>

#include "registers.h"

#ifndef REGISTER_MAP_H
#define REGISTER_MAP_H

#define MAX_SRS_SIZE 65504

namespace srs {
    namespace registers {

        class RegisterMap {

            public:

                /**
                 * Reverse register maps
                 */
                RegisterMap() {
                    for (std::map<int, std::string>::iterator i = peripherals.begin(); i != peripherals.end(); i++) {
                        m_reverse_peripherals[i->second] = i->first;
                    }
                    for (std::map<uint32_t, std::string>::iterator i = commands.begin(); i != commands.end(); i++) {
                        m_reverse_commands[i->second] = i->first;
                    }
                    for (auto peripheral : addresses) {
                        for (std::map<uint32_t, std::string>::iterator i = peripheral.second.begin(); i != peripheral.second.end(); i++) {
                            m_reverse_addresses[peripheral.first][i->second] = i->first;
                        }
                    }
                }

                std::string getPeripheral(int port) {
                    return peripherals.at(port);
                }

                int getPeripheral(std::string name) {
                    return m_reverse_peripherals.at(name);
                }

                /**
                 * SubAddress is ignored for now
                 */
                uint32_t getSubaddress(std::string name) {
                    return 0;
                }

                std::string getCommand(uint32_t command_word) {
                    return commands.at(command_word);
                }

                uint32_t getCommand(std::string name) {
                    return m_reverse_commands.at(name);
                }

                uint32_t getAddress(int port, std::string address_name) {
                    return m_reverse_addresses.at(port).at(address_name);
                }

                std::string getAddress(int port, uint32_t address) {
                    return addresses.at(port).at(address);
                }

            private:

                std::map<std::string, int> m_reverse_peripherals;
                std::map<std::string, uint32_t> m_reverse_commands;
                std::map<int, std::map<std::string, uint32_t>> m_reverse_addresses;

        };
    }
}

#endif
