# SRS APV slow control

This package is written to provide a user-friendly command for the slow control of the SRS.

## Compiling

```bash
cmake -CMAKE_POSITION_INDEPENDENT_CODE=ON -S . -B build
cmake --build build
```

## Usage

### How to configure a FEC

Example command:

```bash
build/srs-configure <FEC_IP_ADDRESS> examples/fec.txt [--verbose] [--external-trigger] [--acquisition-on]
```

Note: the configuration file must be created with SRSDCS.

### How to send a single slow control request

1. Export the python slow control library:
    ```bash
    export PYTHONPATH=$PYTHONPATH:$PWD/build
    ```

2. Use the prompt in `tools/reg.py` to send requests. The request syntax is
    ```
    request <FEC_IP_ADDRESS> <COMMAND> <PERIPHERAL> <SUBADDRESS< <PAYLOAD ...>
    ```

Example of request and response:
```
> request 10.0.2.2 read_list SYS_PORT UNUSED FPGA_IP
0x00000001 read_list SYS_PORT 0 0xa000202
```

## To be implemented

- Complete register address table with subaddresses (only a few peripherals added for now)
- Improve parsing of slow control payload data
- Support parsing slow control error packets

## References

- [SRS slow control manual](https://espace.cern.ch/rd51-wg5/srs/Documentation/SRS_Slow_Control_Manual.pdf)
