#include <map>
#include <string>

#ifndef REGISTERS_H
#define REGISTERS_H

namespace srs {
    namespace registers {

        std::map<int, std::string> peripherals {
            {6007, "SYS_PORT"},
            {6023, "FEC_BI2C_PORT"},
            {6024, "FEC_AI2C_PORT"},
            {6039, "APVAPP_PORT"},
            {6040, "APZRAM_PORT"},
            {6263, "APV_PORT"},
            {6519, "ADCCARD_PORT"},
            {6520, "ADCCTRL_PORT"}
        };

        std::map<uint32_t, std::string> commands {
            {0xaaaaffff, "write_pairs"},
            {0xaabbffff, "write_burst"},
            {0xbbbbffff, "read_burst"},
            {0xbbaaffff, "read_list"}
        };

        std::map<int, std::map<uint32_t, std::string>> subaddresses {
            {6263, {}}
        };

        std::map<int, std::map<uint32_t, std::string>> addresses {
            {6007, {
                   {0, "VERSION"},
                   {1, "FPGAMAC_VENDORID"},
                   {2, "FPGAMAC_ID"},
                   {3, "FPGA_IP"},
                   {4, "DAQPORT"},
                   {5, "SCPORT"},
                   {6, "FRAMEDLY"},
                   {7, "TOTFRAMES"},
                   {8, "ETHMODE"},
                   {9, "SCMODE"},
                   {10, "DAQ_IP"},
                   {11, "DTCC_CTRL"},
                   {12, "MCLK_SEL"},
                   {13, "MCLK_STATUS"},
                   {15, "VERSION_HW"},
                   {0xFFFFFFFF, "SYS_RSTREG"}
               }
            },
            {6039, {
                   {0, "BCLK_MODE"},
                   {1, "BCLK_TRGBURST"},
                   {2, "BCLK_FREQ"},
                   {3, "BCLK_TRGDELAY"},
                   {4, "BCLK_TPDELAY"},
                   {5, "BCLK_ROSYNC"},
                   {7, "ADC_STATUS"},
                   {8, "EVBLD_CHENABLE"},
                   {9, "EVBLD_DATALENGTH"},
                   {10, "EVBLD_MODE"},
                   {11, "EVBLD_EVENTINFOTYPE"},
                   {12, "EVBLD_EVENTINFODATA"},
                   {15, "RO_ENABLE"},
                   {0xFFFFFFFF, "RST_REG"},
                   {0x10, "APZ_SYNC_DET"},
                   {0x11, "APZ_STATUS"},
                   {0x12, "APZ_APVSELECT"},
                   {0x13, "APZ_NSAMPLES"},
                   {0x14, "APZ_SEROSUPP_THR"},
                   {0x15, "APZ_SEROSUPP_PRMS"},
                   {0x1D, "APV_SYNC_LOWTHR"},
                   {0x1E, "APV_SYNC_HIGHTHR"},
                   {0x1F, "APZ_CMD"}
               }
            },
            {6263, {
                   {0x0, "ERROR"},
                   {0x1, "MODE"},
                   {0x2, "LATENCY"},
                   {0x3, "MUXGAIN"},
                   {0x10, "IPRE"},
                   {0x11, "IPCASC"},
                   {0x12, "IPSF"},
                   {0x13, "ISHA"},
                   {0x14, "ISSF"},
                   {0x15, "IPSF"},
                   {0x16, "IMUXIN"},
                   {0x18, "ICAL"},
                   {0x19, "VPSP"},
                   {0x1A, "VFS"},
                   {0x1B, "VFP"},
                   {0x1C, "CFRV"},
                   {0x1D, "CSEL"}
                   }
            },
            {6519, {
                   {0, "HYBRID_RST_IN"},
                   {1, "PWRDOWN_CH0"},
                   {2, "PWRDOWN_CH1"},
                   {3, "EQ_LEVEL_0"},
                   {4, "EQ_LEVEL_1"},
                   {5, "TRGOUT_ENABLE"},
                   {6, "BCLK_ENABLE"}
                   }
            }
        };


    }
}

#endif
