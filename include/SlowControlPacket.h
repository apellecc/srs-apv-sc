#include <vector>
#include <arpa/inet.h>

#include <fmt/core.h>

#include "registers.h"

#ifndef SRS_RESPONSE_H
#define SRS_RESPONSE_H

#define MAX_SRS_SIZE 65504

enum SlowControlPacketType { REQUEST, RESPONSE, ERROR };

class SlowControlPacket {

    public:

        SlowControlPacket(int port, std::vector<uint32_t> words, SlowControlPacketType type) {
            m_port = port;
            m_words = words;
            m_type = type;
        }

        SlowControlPacket(int port, uint32_t *buffer, int size, SlowControlPacketType type) {
            m_port = port;
            for (int i=0; i<size/4; i++) {
                m_words.push_back(htonl(*buffer));
                buffer++;
            }
            buffer -= (int) size;
            m_type = type;
        }

        uint32_t getRequestID() { return m_words.at(0); }
        uint32_t getSubAddress() { return m_words.at(1); }
        uint32_t getCommand() { return m_words.at(2); }
        uint32_t getCommandInfo() { return m_words.at(3); }

        int port() { return m_port; }
        int size() { return m_words.size(); }
        void words(char *buffer) {
            uint32_t *buf;
            buf = (uint32_t *) buffer;
            for (int i=0; i<m_words.size(); i++) {
                buf[i] = htonl(m_words.at(i));
            }
        }
        
        std::vector<uint32_t> getPayload() {
            std::vector<uint32_t>::const_iterator first = m_words.begin() + 4;
            std::vector<uint32_t>::const_iterator last = m_words.end();
            return std::vector<uint32_t>{first, last};
        }

        std::string getPeripheral() {
            return srs::registers::peripherals.at(m_port);
        }

        std::string getCommandName() {
            return srs::registers::commands.at(getCommand());
        }

        std::string getSubAddressName() {
            try {
                return srs::registers::subaddresses.at(m_port).at(getSubAddress());
            } catch (std::out_of_range &e) {
                return std::to_string(getSubAddress());
            }
        }

        /**
         * Return register name corresponding to peripheral and address;
         * if the register is not mapped, return the address as string
         */
        std::string getAddressName(uint32_t address) {
            try {
                return srs::registers::addresses.at(m_port).at(address);
            } catch (std::out_of_range &e) {
                return std::to_string(address);
            }
        }

        /**
         * Return if response packet has at least one error
         */
        bool getError() {
            if (m_type==SlowControlPacketType::RESPONSE) {
                bool is_error = false;
                std::vector<uint32_t> payload = getPayload();
                for (int i=0; i<payload.size(); i+=2) {
                    is_error |= payload.at(i) != 0;
                }
                return is_error;
            }
            throw std::domain_error{"Cannot get error from non-response packet"};
             
        }

        std::string to_string() {
            std::string command = fmt::format("{:#010x} {} {} {} ", getRequestID(), getCommandName(), getPeripheral(), getSubAddressName());
            std::vector<uint32_t> payload = getPayload();
            switch(m_type) {
                case SlowControlPacketType::REQUEST:
                    if (getCommandName()=="write_pairs") {
                        for (int i=0; i<payload.size(); i+=2) {
                            command += fmt::format("{} {:#x} ", getAddressName(payload.at(i)), payload.at(i+1));
                        }
                    } else if (getCommandName()=="read_list") {
                        for (int i=0; i<payload.size(); i++) {
                            command += fmt::format("{} ", getAddressName(payload.at(i)));
                        }
                    } else { // includes write_burst and read_burst
                        for (int i=0; i<payload.size(); i++) {
                            command += fmt::format("{:#x} ", payload.at(i));
                        }
                    }
                    break;
                case SlowControlPacketType::RESPONSE:
                    for (int i=0; i<payload.size(); i+=2) {
                        command += fmt::format("{:#x} ", payload.at(i+1));
                    }
                    if (getError()) {
                        command += "ERROR!";
                    }
                    break;
                default:
                    for (int i=0; i<payload.size(); i+=2) {
                        command += fmt::format("{:#x} {:#x} ", payload.at(i), payload.at(i+1));
                    }
            }

            return command;
        }

    private:
        int m_port;
        std::vector<uint32_t> m_words;
        SlowControlPacketType m_type;

};

#endif
