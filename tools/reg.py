import cmd
import sys

import pysrs

class SRSPrompt(cmd.Cmd):

    intro = "Example command: request 10.0.2.2 read_list SYS_PORT UNUSED VERSION"
    prompt = "> "

    def do_EOF(self, args):
        print("")
        return True

    def do_exit(self, args):
        return True

    def do_request(self, args):
        hostname = args.split(" ")[0]
        request_str = " ".join(args.split(" ")[1:])
        response = pysrs.request(hostname, request_str)
        if "ERROR!" in response:
            print("\033[91m{}\033[0m".format(response))
        else:
            print(response)

    def complete_request(self, text, line, begidx, endidx):
        return ["write_pairs", "write_burst", "read_burst", "read_list"]

if __name__ == "__main__":
    SRSPrompt().cmdloop()
