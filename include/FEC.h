#include <iostream>
#include <fstream>
#include <sstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>

#include <fmt/core.h>

#include "SlowControlPacket.h"
#include "RegisterMap.h"

#ifndef FEC_H
#define FEC_H

class connection_error : public std::exception {
    private:
        std::string message;

    public:
        connection_error(std::string msg) : message(msg){}
        std::string what() {
            return message;
        }
};

/**
 * Read 16-bit word from binary stream
 */
uint16_t read_word16(std::ifstream &f) {
    uint16_t word;
    f.read(reinterpret_cast<char*>(&word), sizeof(word));
    return htons(word);
}

/**
 * These two functions are used to handle the name or the IP of the machine
 */
struct addrinfo *host_serv(const char *host, const char *serv, int family, int socktype) {
    int n;
    struct addrinfo hints, *res;

    bzero(&hints,sizeof(struct addrinfo));
    hints.ai_flags = AI_CANONNAME; // returns canonical name
    hints.ai_family = family; // AF_UNSPEC AF_INET AF_INET6 etc...
    hints.ai_socktype = socktype; // 0 SOCK_STREAM SOCK_DGRAM

    if (getaddrinfo(host,serv,&hints,&res) != 0) return NULL;

    return (res);
}

char* sock_ntop_host(const struct sockaddr *sa, socklen_t salen) {
    static char str[128];		/* Unix domain is largest */

    switch (sa->sa_family) {
        case AF_INET: {
                          struct sockaddr_in	*sin = (struct sockaddr_in *) sa;

                          if (inet_ntop(AF_INET, &sin->sin_addr, str, sizeof(str)) == NULL)
                              return(NULL);
                          return(str);
                      }
        default:
                      snprintf(str, sizeof(str), "sock_ntop_host: unknown AF_xxx: %d, len %d",
                              sa->sa_family, salen);
                      return(str);
    }
    return (NULL);
}

class FEC {

    public:

        FEC(std::string hostname) {
            m_hostname = hostname;
            bind_port();
        }

        ~FEC() {
            close(m_sockfd);
        }

        /**
         * Create and bind to UDP socket
         */
        void bind_port() {

            m_sockfd = socket(AF_INET,SOCK_DGRAM,0);
            const int on = 1; /* Why broadcast? Check */
            setsockopt(m_sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));

            bzero(&m_servaddr,sizeof(m_servaddr));
            m_servaddr.sin_family = AF_INET;
            m_servaddr.sin_port = htons(daq_port);
            int bnd = bind(m_sockfd,(struct sockaddr *)& m_servaddr, sizeof(m_servaddr));

            if (bnd==-1) {
                throw connection_error{"Could not bind to port 6007. Is another slow control software running?"};
            }
        }

        /**
         * Read UDP packet from binary configuration file
         */
        SlowControlPacket read_packet_file(std::ifstream &config_file, int n_words) {

            uint16_t packet_port = read_word16(config_file);
            uint16_t packet_id = read_word16(config_file);

            uint32_t *buf;
            char g_buffer[MAX_SRS_SIZE];
            int g_byte = 0;
            bzero(g_buffer, MAX_SRS_SIZE);
            buf = (uint32_t *) g_buffer;

            uint32_t word32;
            for (int i=0; i<n_words; i++) {
                config_file.read(reinterpret_cast<char*>(buf), sizeof(*buf));
                buf++;
                g_byte += 4;
            }

            buf -= n_words;
            return SlowControlPacket{packet_port, buf, g_byte, SlowControlPacketType::REQUEST};
        }

        /**
         * Send UDP packet request to FEC
         */
        void send_request(SlowControlPacket request) {
            m_servaddr.sin_port = htons(request.port());
            m_ai = host_serv(m_hostname.c_str(), NULL, AF_INET, SOCK_DGRAM);
            m_h = sock_ntop_host(m_ai->ai_addr,m_ai->ai_addrlen);
            inet_pton(AF_INET,(m_ai->ai_canonname ? m_h : m_ai->ai_canonname),&m_servaddr.sin_addr);
            socklen_t g_servlen = sizeof(m_servaddr);
            struct sockaddr *g_replyaddr;
            g_replyaddr = (struct sockaddr *) malloc(g_servlen);
            freeaddrinfo(m_ai);

            char buffer[MAX_SRS_SIZE] = {0};
            request.words(buffer);

            int send_code = sendto(m_sockfd, buffer, 4*request.size(), 0, (struct sockaddr *)&m_servaddr, sizeof(m_servaddr));
            if (send_code==-1) {
                throw connection_error{"Error " + std::to_string(errno) + " sending data to FEC."};
            }

            m_last_reqid = request.getRequestID();
        }

        /**
         * Receive UDP response from FEC
         */
        SlowControlPacket receive_reply(int port) {
            char rcv_buffer[MAX_SRS_SIZE];
            bzero(rcv_buffer, MAX_SRS_SIZE);
            uint32_t *rcv_buf;
            struct sockaddr_in recv_sockaddr;
            socklen_t recv_addr_size = sizeof(recv_sockaddr);
            int rcv_size = recvfrom(m_sockfd, rcv_buffer, sizeof(rcv_buffer), 0, (struct sockaddr*) &recv_sockaddr, &recv_addr_size);
            rcv_buf = (uint32_t *) rcv_buffer;
            return SlowControlPacket{port, rcv_buf, rcv_size, SlowControlPacketType::RESPONSE};
        }

        /**
         * Compile a request string to slow control packet
         */
        SlowControlPacket compile_string(std::string request_str) {
            std::vector<std::string> command_tokens;
            std::istringstream f{request_str};
            std::string tok;
            while (getline(f, tok, ' ')) {
                command_tokens.push_back(tok);
            }
            int port = m_registers.getPeripheral(command_tokens.at(1));

            std::vector<uint32_t> packet_words;
            packet_words.push_back(m_last_reqid + 1); // request ID
            uint32_t subaddress = m_registers.getSubaddress(command_tokens.at(2));
            packet_words.push_back(subaddress);
            packet_words.push_back(m_registers.getCommand(command_tokens.at(0))); // command field 1
            packet_words.push_back((uint32_t) 0); // command field 2, ignored for now
            /**
             * Append command address and data
             */
            for (int i=3; i<command_tokens.size(); i++) {
                /**
                 * Try to parse the address name;
                 * if the address is not found, assume the command contains the bare address number
                 */
                try {
                    packet_words.push_back(
                            m_registers.getAddress(port, command_tokens.at(i).c_str())
                            );
                } catch (std::out_of_range &e) {
                    packet_words.push_back((uint32_t) std::strtoul(command_tokens.at(i).c_str(), NULL, 0));
                }
            }
            return SlowControlPacket{port, packet_words, SlowControlPacketType::REQUEST};
        }

        /**
         * Send a request to write registers and read back reply
         */
        std::string slow_control_request(std::string request_str) {
            SlowControlPacket request = compile_string(request_str);
            send_request(request);
            SlowControlPacket response = receive_reply(request.port());
            return response.to_string();
        }

        /**
         * Enable or disable external trigger
         */
        std::string set_external_trigger(bool enabled) {
            std::string response;
            if (enabled) {
                response = slow_control_request("write_pairs APVAPP_PORT UNUSED 0 0x4");
            } else {
                /**
                 * Warning: this enables the internal trigger.
                 * Is this the desired behaviour?
                 */
                response = slow_control_request("write_pairs APVAPP_PORT UNUSED 0 0x0");
            }
            return response;
        }

        /**
         * Enable or disable external trigger
         */
        std::string set_readout_enable(bool enabled) {
            std::string response;
            if (enabled) {
                response = slow_control_request("write_pairs APVAPP_PORT UNUSED 0xF 1");
            } else {
                response = slow_control_request("write_pairs APVAPP_PORT UNUSED 0xF 0x0");
            }
            return response;
        }

        /**
         * Configure FEC from binary configuration file generated by SRSDCS
         * Heavily copied from slow_control.c
         */
        void configure_from_file(std::string config_path, bool verbose) {

            /**
             * Read configuration file
             */
            std::ifstream config_file{config_path.c_str(), std::ios::binary};
            if (!config_file.is_open()) {
                throw connection_error{"Unable to read file " + config_path};
            }
            int config_n_words[4] = {18, 36, 26, 8};
            int config_start[4] = {0, 76, 76+148, 76+148+108};
            for (int i_cmd=0; i_cmd<4; i_cmd++) {

                config_file.seekg(config_start[i_cmd]);
                SlowControlPacket request = read_packet_file(config_file, config_n_words[i_cmd]);

                if (verbose) {
                    fmt::print("Request:  {}\n", request.to_string());
                }

                send_request(request);

                SlowControlPacket response = receive_reply(request.port());

                if (verbose) {
                    fmt::print("Response: {}\n", response.to_string());
                }
                sleep(0.5);

                /**
                 * Reset APV
                 */
                if (i_cmd==1) {
                    slow_control_request("write_pairs APVAPP_PORT UNUSED 0xFFFFFFFF 1");
                    sleep(0.5);
                }
            }

            config_file.close();

        }

    private:

        struct addrinfo *m_ai;
        char *m_h;
        struct sockaddr_in m_servaddr;

        std::string m_hostname;
        int m_sockfd;

        const int daq_port = 6007;

        uint32_t m_last_reqid = 0x80000000;

        srs::registers::RegisterMap m_registers;
};

#endif
