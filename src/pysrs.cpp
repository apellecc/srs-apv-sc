#include <pybind11/pybind11.h>

#include "FEC.h"

namespace py = pybind11;

std::string request(std::string hostname, std::string expression) {
    FEC fec{hostname};
    std::string response;
    try {
        response = fec.slow_control_request(expression);
    } catch (std::exception &e) {
        return e.what();
    }
    return response;
}

PYBIND11_MODULE(pysrs, m) {
    m.def("request", &request, "Send a slow control request the the FEC",
            py::arg("hostname"), py::arg("expression"));
}
